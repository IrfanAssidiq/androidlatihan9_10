package irfan.sampling.androidlatihan9.views.views_add

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import irfan.sampling.androidlatihan9.R


/**
 *   created by Irfan Assidiq on 1/1/18
 *   email : assidiq.irfan@gmail.com
 **/
class AddPeopleData : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_add_people)
    }
}