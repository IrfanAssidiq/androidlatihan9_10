package irfan.sampling.androidlatihan9.views.views_details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import irfan.sampling.androidlatihan9.R


/**
 *   created by Irfan Assidiq on 3/31/19
 *   email : assidiq.irfan@gmail.com
 **/
class DetailAct : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_detail)
    }
}