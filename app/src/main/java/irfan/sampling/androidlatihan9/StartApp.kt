package irfan.sampling.androidlatihan9

import android.app.Application
import irfan.sampling.androidlatihan9.data.ContactRepo


/**
 *   created by Irfan Assidiq on 3/30/19
 *   email : assidiq.irfan@gmail.com
 **/
class StartApp : Application(){
    fun getPeopleRepo() = ContactRepo(this)
}