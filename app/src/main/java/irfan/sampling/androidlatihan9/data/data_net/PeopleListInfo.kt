package irfan.sampling.androidlatihan9.data.data_net

import irfan.sampling.androidlatihan9.data.data_model.People


/**
 *   created by Irfan Assidiq on 3/30/19
 *   email : assidiq.irfan@gmail.com
 **/
class PeopleListInfo {
    companion object {
        var peopleList = initPeopleList()

        /**
         * add dummy data
         */

        private fun initPeopleList() : MutableList<People>{
            var po_peoples = mutableListOf<People>()
            po_peoples.add(
                People(
                "Irfan",
                "Depok",
                "083874665415",
                "assidiq.irfan@gmail.com",
                "fb.me/irfan",
                "twitter.com/irfan",
                1))
            po_peoples.add(People(
                "Irfan2",
                "Depok2",
                "083874665415",
                "assidiq.irfan@gmail.com",
                "fb.me/irfan",
                "twitter.com/irfan",
                2))
            po_peoples.add(People(
                "Irfan3",
                "Depok3",
                "083874665415",
                "assidiq.irfan@gmail.com",
                "fb.me/irfan",
                "twitter.com/irfan",
                3))
            return po_peoples
        }
    }
}