package irfan.sampling.androidlatihan9.data

import android.app.Application
import android.arch.lifecycle.LiveData
import irfan.sampling.androidlatihan9.data.data_model.People
import irfan.sampling.androidlatihan9.data.data_net.PeopleListInfo
import irfan.sampling.androidlatihan9.data.db_only.PeopleDAO
import irfan.sampling.androidlatihan9.data.db_only.PeopleDb


/**
 *   created by Irfan Assidiq on 3/30/19
 *   email : assidiq.irfan@gmail.com
 **/
class ContactRepo(application: Application) {

    private val peopleDao : PeopleDAO

    init {
        val peopleDatabase = PeopleDb.getInstance(application)
        peopleDao = peopleDatabase.peopleDao()
    }

    /**
     * menampilkan data tapi secara descending
     */
    fun getAllPeople() : LiveData<List<People>>{
//        val allpeople = PeopleListInfo.peopleList
//        return allpeople.reversed()
        return peopleDao.getAll()
    }

    fun insertPeople(people: People){
//        PeopleListInfo.peopleList.add(people)
        peopleDao.insert(people)
    }

    fun findPeople(id : Int): LiveData<People>{
//        for (people in PeopleListInfo.peopleList){
//            if(people.id == id){
//                return people
//            }
//        }
//        return null
        return peopleDao.find(id)
    }

}