package irfan.sampling.androidlatihan9.data.data_model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


/**
 *   created by Irfan Assidiq on 3/30/19
 *   email : assidiq.irfan@gmail.com
 **/
@Entity
class People (
    var name : String = "",
    var metAt : String = "",
    var contact : String = "",
    var email : String = "",
    var facebook : String = "",
    var twitter : String = "",
    @PrimaryKey(autoGenerate = true) var id : Int = 0
)